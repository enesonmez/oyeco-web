const express = require('express');
const router = express.Router();
const app = express();
const expressEjsLayout = require('express-ejs-layouts')

app.use(express.static("public"))
app.use("/css",express.static(__dirname + "public/css"))
app.use("/js",express.static(__dirname + "public/js"))
app.use("/fonts",express.static(__dirname + "public/fonts"))

//EJS
app.set("views","./views")
app.set('view engine', 'ejs');
app.use(expressEjsLayout);
//BodyParser
app.use(express.urlencoded({ extended: false }));
global.isSignedin = false;
global.Userinfo = '';
global.mylistofWorkers='';
global.globalMethods = {
    girisKontrolu(res) {
        if (!global.isSignedin) {
            res.render('login');
            return false;
        } else {
            return true;
        }
    }
}
//Routes
app.use('/', require('./routes/giris'));
//app.use('/users', require('./routes/users'));
app.use('/mainpage', require('./routes/mainpage'));
app.use('/calisanyonetimi', require('./routes/calisanyonetimi'));
app.use('/kullaniciyonetimi', require('./routes/kullaniciyonetimi'));
app.use('/rotayonetimi', require('./routes/rotayonetimi'));


app.listen(1337); 