const express = require('express');
const router = express.Router();
const axios = require('axios');
const https = require('https')
var bodyParser = require('body-parser');
const { route } = require('./giris');

global.kullaniciYonetimiApiCalls = {
    kullaniciYonetimiMainPage(res) {
        var mylistofUsers;
        axios({
            method: 'get',
            url: 'https://appupcycling.herokuapp.com/api/users',
            headers: {
                "Content-type": "application/json",
                "api-key": "captainjacksparrowsayshi"
            }
        }).catch((error) => {
            res.render("mainpage");
        }).then((response) => {
            mylistofUsers = response.data.content;
            console.log(mylistofUsers);
            res.render('kullaniciyonetimi', { 'userList': mylistofUsers });
        });
    },
    kullaniciYasakla(req, res) {
        parsedInfo = JSON.parse(req.body.action);
        axios({
            method: 'put',
            url: 'https://appupcycling.herokuapp.com/api/users/updateBlock/' + parsedInfo.id,
            data: {
                "isBlock": (!parsedInfo.isBlock),
            },
            headers: {
                "Content-type": "application/json",
                "api-key": "captainjacksparrowsayshi"
            }
        }).then((response) => {
            global.kullaniciYonetimiApiCalls.kullaniciYonetimiMainPage(res);
        });
    },
    kullaniciDuzenle(req, res) {
        axios({
            method: 'put',
            url: 'https://appupcycling.herokuapp.com/api/users/update/' + req.body.id,
            data: {
                "firstName": req.body.firstName,
                "lastName": req.body.lastName,
                "phoneNumber": req.body.phoneNumber,
                "gender": req.body.gender,
                "birthDay": '1998-11-12T00:00:00Z',
            },
            headers: {
                "Content-type": "application/json",
                "api-key": "captainjacksparrowsayshi"
            }
        }).then((response) => {
            global.kullaniciYonetimiApiCalls.kullaniciYonetimiMainPage(res);
        });
    }
};

router.post('/kullaniciyasakla', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
       global.kullaniciYonetimiApiCalls.kullaniciYasakla(req, res);
    }
})

router.post('/kullaniciduzenle', (req, res, next) => {

    if (req.body.action === "iptal") {
        global.kullaniciYonetimiApiCalls.kullaniciYonetimiMainPage(res);
    } else if (req.body.action === "degistir") {
        global.kullaniciYonetimiApiCalls.kullaniciDuzenle(req, res);
    } else {
        res.render('kullaniciduzenle', { myValuesfromKullaniciyonetimi: JSON.parse(req.body.action) });
    }
})


router.get('/kullaniciduzenle', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('kullaniciduzenle');
    }
})



router.get('/*', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('mainpage');
    }
})
module.exports = router;
