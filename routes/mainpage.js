const express = require('express');
const router = express.Router();
const axios = require('axios');
const { route } = require('./giris');


//logout
router.get('/logout', (req, res) => {
    global.isSignedin = false;
    res.render('login');
})

router.get('/calisanyonetimi', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        global.calisanYonetimiApiCalls.calisanYonetimiMainPage(res);
    }
})

router.get('/kullaniciyonetimi', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        global.kullaniciYonetimiApiCalls.kullaniciYonetimiMainPage(res);
    }
})


router.get('/rotayonetimi', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        global.rotaYonetimiApiCalls.rotaYonetimiMainPage(res);
        console.log(global.mylistofWorkers);
    }
})

router.get('/*', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('mainpage');
    }
})

module.exports = router;