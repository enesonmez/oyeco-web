const express = require('express');
const router = express.Router();
const axios = require('axios');

//login page
router.get('/', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('login');
        }
})
router.post('/mainpage', (req, res, next) => {//'login'di
    axios({
        method: 'post',
        url: 'https://appupcycling.herokuapp.com/api/manageworkers/signin',
        data: {
            "email": req.body.email,
            "password": req.body.password,
        },
        headers: {
            "Content-type": "application/json",
            "api-key": "captainjacksparrowsayshi"
        }
    }).catch((error) => {
        res.render('login');
    }).then((response) => {
        global.userInfo = response.data;
        global.isSignedin = true;
        console.log(global.userInfo);
        res.render('mainpage');
    });
    console.log(global.isSignedin);
})

module.exports = router; 
