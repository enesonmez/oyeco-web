const express = require('express');
const router = express.Router();
const axios = require('axios');
const { route } = require('./giris');


global.rotaYonetimiApiCalls = {
    rotaYonetimiMainPage(res) {
        var myListofWorkers;
        var myListofRequests;
        var getWorkersDone = false;
        var getRequestsDone = false;
            axios({
                method: 'get',
                url: 'https://appupcycling.herokuapp.com/api/requests',
                headers: {
                    "Content-type": "application/json",
                    "api-key": "captainjacksparrowsayshi"
                }
            }).then((response) => {
                myListofRequests = response.data.content;
                getRequestsDone = true;
                    if (getWorkersDone) {
                        res.render('rotayonetimi', { 'workerList': myListofWorkers, 'requestList': myListofRequests });
                    }
            });
            axios({
                method: 'get',
                url: 'https://appupcycling.herokuapp.com/api/fieldworkers',
                headers: {
                    "Content-type": "application/json",
                    "api-key": "captainjacksparrowsayshi"
                }
            }).then((response) => {
                myListofWorkers = response.data.content;
                getWorkersDone = true;
                if (getRequestsDone) {
                        res.render('rotayonetimi', { 'workerList': myListofWorkers, 'requestList': myListofRequests});
                }
            });
    },
    rotaOlustur(req, res){
        var parsedBody = JSON.parse(req.body.selectpicker);
        var { checkboxes } = req.body;
        var arrayToJSON = [];
        for (var i = 0; i < checkboxes.length; i++) {
            arrayToJSON.push({ "requestID": JSON.parse(checkboxes[i]) });
        }
        console.log(arrayToJSON);
        var gonderilecekIstekSayisi = checkboxes.length;
        var tamamlanmisisteksayisi = 0;
        axios({
                method: 'post',
                url: 'https://appupcycling.herokuapp.com/api/routes/register',
                data: {
                    "fieldWorkerID": parsedBody.wID,
                    "routeAddressMaps": arrayToJSON,
                },
                headers: {
                    "Content-type": "application/json",
                    "api-key": "captainjacksparrowsayshi"
                }
            }).then((response) => {
                global.rotaYonetimiApiCalls.rotaYonetimiMainPage(res);
            })
    }
};


router.post('/rotaolustur', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        global.rotaYonetimiApiCalls.rotaOlustur(req, res);
    }
})



router.get('/*', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('mainpage');
    }
})
module.exports = router;
