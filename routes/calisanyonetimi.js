const express = require('express');
const router = express.Router();
const axios = require('axios');
const { route } = require('./giris');

global.calisanYonetimiApiCalls = {
    calisanYonetimiMainPage(res) {
        var mylistofWorkers;
        axios({
            method: 'get',
            url: 'https://appupcycling.herokuapp.com/api/fieldworkers',
            headers: {
                "Content-type": "application/json",
                "api-key": "captainjacksparrowsayshi"
            }
        }).catch((error) => {
            res.render("mainpage");
        }).then((response) => {
            mylistofWorkers = response.data.content;
            res.render('calisanyonetimi', { 'workerList': mylistofWorkers });
        });
    },
    calisanSil(req, res) {
        console.log(req.body.action);
        axios({
            method: 'delete',
            url: 'https://appupcycling.herokuapp.com/api/fieldworkers/' + req.body.action,
            headers: {
                "Content-type": "application/json",
                "api-key": "captainjacksparrowsayshi"
            }
        }).catch((error) => {
            res.render('mainpage');
            console.log(error);
        }).then((response) => {
            global.calisanYonetimiApiCalls.calisanYonetimiMainPage(res);
        });
    },
    calisanEkle(req, res) {
        axios({
            method: 'post',
            url: 'https://appupcycling.herokuapp.com/api/fieldworkers/register',
            data: {
                "firstName": req.body.firstName,
                "lastName": req.body.lastName,
                "phoneNumber": req.body.phoneNumber,
                "email": req.body.email,
                "password": req.body.password,
                "gender": req.body.gender,
                "birthDay": '1998-11-12T00:00:00Z',
            },
            headers: {
                "Content-type": "application/json",
                "api-key": "captainjacksparrowsayshi"
            }
        }).catch((error) => {
            res.render('mainpage');
            console.log(error);
        }).then((response) => {
            global.calisanYonetimiApiCalls.calisanYonetimiMainPage(res);
        });
    },
    calisanduzenle(req, res) {
        axios({
            method: 'put',
            url: 'https://appupcycling.herokuapp.com/api/fieldworkers/' + req.body.wID,
            data: {
                "firstName": req.body.firstName,
                "lastName": req.body.lastName,
                "phoneNumber": req.body.phoneNumber,
                "email": req.body.email,
                "password": req.body.password,
                "birthDay": '1998-11-12T00:00:00Z',
            },
            headers: {
                "Content-type": "application/json",
                "api-key": "captainjacksparrowsayshi"
            }
        }).then((response) => {
            global.calisanYonetimiApiCalls.calisanYonetimiMainPage(res);
        });
    }
};


router.get('/calisanyonetimi', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        global.calisanYonetimiApiCalls.calisanYonetimiMainPage(res);
    }
})
router.post('/calisanisil', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        global.calisanYonetimiApiCalls.calisanSil(req, res);
    }
})

router.post('/calisanekle', (req, res, next) => {
    if (global.globalMethods.girisKontrolu(res)) {
        if (req.body.action === "iptal") {
            global.calisanYonetimiApiCalls.calisanYonetimiMainPage(res);
        } else if (req.body.action === "ekle") {
            global.calisanYonetimiApiCalls.calisanEkle(req, res);
        }
    }
})


router.post('/calisanduzenle', (req, res, next) => {
    if (global.globalMethods.girisKontrolu(res)) {
        if (req.body.action === "iptal") {
            global.calisanYonetimiApiCalls.calisanYonetimiMainPage(res);
        } else if (req.body.action === "degistir") {
            global.calisanYonetimiApiCalls.calisanduzenle(req, res);
        } else {
            res.render('calisanduzenle', { myValuesfromCalisanyonetimi: JSON.parse(req.body.action) });
        }
    }
})


router.get('/calisanyonetimi', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('calisanyonetimi');
    }
})


router.get('/calisanekle', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('calisanekle');
    }
})

router.get('/*', (req, res) => {
    if (global.globalMethods.girisKontrolu(res)) {
        res.render('mainpage');
    }
})
module.exports = router;
